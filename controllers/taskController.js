// controllers - business logic
const Task = require("../models/task")

// controller for getting all tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	});
}

// creating tasks
module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

// deleting Tasks
module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removeTask, err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			return removeTask;
		}
	})
}

// updating Tasks

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false;
		}else {
			result.name = newContent.name

			return result.save().then((updatedTask, saveErr) => {

				if(saveErr) {
					console.log(saveErr)
					return false;
				} else {
					return updatedTask;
				}
			})
		}
	})
}

// ACTIVITY s36

// Get a specific task

module.exports.getOneTask = (taskId) => {

	return Task.findById(taskId).then((result) => {
		return result
	})
}

// Changing the status

module.exports.updateStatus = (taskId, newContent) => {

	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false;
		}else {
			result.status = newContent.status;

			return result.save().then((updatedStatus, saveErr) => {

				if(saveErr) {
					console.log(saveErr)
					return false;
				} else {
					return updatedStatus;
				}
			})
		}
	})
}