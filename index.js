// Express JS Server 

// set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoutes");

// server setup
const app = express();
const port = 3001;
	// middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}))

// database connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.guoofgf.mongodb.net/b244-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true,
	}
);

app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now listening to port ${port}`))