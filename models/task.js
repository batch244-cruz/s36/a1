// schema - blueprints
const mongoose = require('mongoose')

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});
// model name should be capitalzied and singular in from
module.exports = mongoose.model("Task", taskSchema);